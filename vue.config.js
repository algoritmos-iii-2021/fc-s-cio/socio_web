module.exports = {
    publicPath: process.env.NODE_ENV === 'production'
      ? '/socio_web/'
      : '/'
  }